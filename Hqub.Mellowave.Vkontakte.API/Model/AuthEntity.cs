﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Hqub.Mellowave.Vkontakte.API.Model
{
    public class AuthEntity : Entity
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
    }
}
