﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Hqub.Mellowave.Vkontakte.API.Model
{
    [XmlRoot("user")]
    public class Profile : Entity
    {
        [XmlElement("uid")]
        public string UID { get; set; }

        [XmlElement("first_name")]
        public string FirstName { get; set; }

        [XmlElement("last_name")]
        public string LastName { get; set; }

        [XmlElement("photo_100")]
        public string Photo100 { get; set; }
    }
}
