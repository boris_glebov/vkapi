﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Hqub.Mellowave.Vkontakte.API.Model
{
    [XmlRoot("response")]
    public class ProfileResponse
    {
        [XmlAttribute("list")]
        public bool IsList { get; set; }

        [XmlElement("user")]
        public List<Profile> Profiles { get; set; } 
    }
}

//<?xml version="1.0" encoding="utf-8"?>
//<response list="true">
// <user>
//  <uid>6666100</uid>
//  <first_name>Борис</first_name>
//  <last_name>Глебов</last_name>
//  <photo_100>http://cs419523.vk.me/v419523100/2407/M5bIUaDnZV8.jpg</photo_100>
// </user>
//</response>
