﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Hqub.Mellowave.Vkontakte.API.Model.Dialogs
{
    [XmlRoot("dialog")]
    public class Dialog : Entity
    {
        [XmlElement("unread")]
        public int UnreadCount { get; set; }

        [XmlElement("message")]
        public Message.Message Message { get; set; }
    }
}
