﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Hqub.Mellowave.Vkontakte.API.Model.Dialogs
{
    [XmlRoot("response")]
    public class DialogResponse : Entity
    {
        [XmlAttribute("list")]
        public bool IsList { get; set; }

        [XmlElement("count")]
        public int Count { get; set; }

        [XmlElement("unread_dialogs")]
        public int UnreadDialogsCount { get; set; }

        [XmlArray("items")]
        [XmlArrayItem("dialog")]
        public List<Dialog> Dialogs { get; set; }
    }
}
