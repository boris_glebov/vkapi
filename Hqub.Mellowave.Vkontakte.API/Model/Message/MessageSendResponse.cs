﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Hqub.Mellowave.Vkontakte.API.Model.Message
{
    [XmlRoot("response")]
    public class MessageSendResponse : Entity
    {
        [XmlText]
        public string Value { get; set; }
    }
}
