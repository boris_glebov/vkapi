﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Hqub.Mellowave.Vkontakte.API.Model.Message
{
    [XmlRoot("response")]
    public class MessageResponse : Entity
    {
        [XmlAttribute("list")]
        public bool IsList { get; set; }

        [XmlElement("count")]
        public int Count { get; set; }

        [XmlArray("items")]
        [XmlArrayItem("message")]
        public List<Message> Messages { get; set; } 
    }
}
