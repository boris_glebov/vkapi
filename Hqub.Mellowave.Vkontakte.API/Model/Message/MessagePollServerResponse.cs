﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Hqub.Mellowave.Vkontakte.API.Model.Message
{
    [XmlRoot("response")]
    public class MessagePollServerResponse : Entity
    {
        [XmlElement("key")]
        public string Key { get; set; }

        [XmlElement("server")]
        public string Server { get; set; }

        [XmlElement("ts")]
        public string Ts { get; set; }

        public override string ToString()
        {
            return string.Format("Key: {0}, Server: {1}, TS: {2}", Key, Server, Ts);
        }
    }
}
