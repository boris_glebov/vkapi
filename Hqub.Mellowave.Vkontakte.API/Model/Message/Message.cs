﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Hqub.Mellowave.Vkontakte.API.Model.Message
{
    [XmlRoot("message")]
    public class Message : Entity
    {
        [XmlElement("id")]
        public int Id { get; set; }

        [XmlElement("date")]
        public long UnixDate { get; set; }

        [XmlElement("out")]
        public bool Out { get; set; }

        [XmlElement("user_id")]
        public int UserId { get; set; }

        [XmlElement("read_state")]
        public bool ReadState { get; set; }

        [XmlElement("title")]
        public string Title { get; set; }

        [XmlElement("body")]
        public string Body { get; set; }
    }
}
