﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Hqub.Mellowave.Vkontakte.API.Model.Audio
{
    [XmlRoot("response")]
    public class AudioManyResponse : Entity
    {
        [XmlAttribute("list")]
        public bool IsList { get; set; }

        [XmlElement("count")]
        public int Count { get; set; }

        [XmlArray("items")]
        [XmlArrayItem("item")]
        public List<Audio> Items { get; set; }

    }
}
