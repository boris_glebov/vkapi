﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Hqub.Mellowave.Vkontakte.API.Model.Audio
{
    [XmlRoot("audio")]
    public class Audio
    {
        [XmlElement("id")]
        public string Id { get; set; }

        [XmlElement("owner_id")]
        public string OwnerId { get; set; }

        [XmlElement("title")]
        public string Title { get; set; }

        [XmlElement("artist")]
        public string Artist { get; set; }

        [XmlElement("duration")]
        public int Duration { get; set; }

        [XmlElement("url")]
        public string Url { get; set; }

        [XmlElement("lyrics_id")]
        public int LyricsId { get; set; }

        [XmlElement("genre_id")]
        public int GenreId { get; set; }

        /// <summary>
        /// For getbyid {owner_id}_{audio_id}
        /// </summary>
        public string ComplexId { get { return string.Format("{0}_{1}", OwnerId, Id); } }

        public override string ToString()
        {
            return string.Format("{0} - {1}", Artist, Title);
        }
    }
}
