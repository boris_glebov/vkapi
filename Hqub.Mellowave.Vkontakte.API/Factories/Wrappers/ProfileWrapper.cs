﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hqub.Mellowave.Vkontakte.API.Model;

namespace Hqub.Mellowave.Vkontakte.API.Factories.Wrappers
{
    public class ProfileWrapper : BaseWrapper
    {
        public ProfileWrapper(string token) : base(token)
        {
        }

        public ProfileResponse Get(string uid)
        {
            var response = RequestHelper.GetMethod<ProfileResponse>("users.get", string.Format("uids={0}&fields=photo_100", uid), Token);

            return response;
        }
    }
}
