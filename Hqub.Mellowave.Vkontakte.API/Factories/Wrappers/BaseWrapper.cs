﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hqub.Mellowave.Vkontakte.API.Factories.Wrappers
{
    public abstract class BaseWrapper
    {
        public BaseWrapper(string token)
        {
            Token = token;
        }

        public string Token { get; set; }

        public bool IsTokenSet { get { return !string.IsNullOrEmpty(Token); } }
    }
}
