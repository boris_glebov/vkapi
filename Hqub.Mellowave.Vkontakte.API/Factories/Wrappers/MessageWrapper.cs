﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hqub.Mellowave.Vkontakte.API.Model.Audio;
using Hqub.Mellowave.Vkontakte.API.Model.Dialogs;
using Hqub.Mellowave.Vkontakte.API.Model.Message;

namespace Hqub.Mellowave.Vkontakte.API.Factories.Wrappers
{
    public class MessageWrapper : BaseWrapper
    {
        public MessageWrapper(string token) : base(token)
        {
        }

        /// <summary>
        /// Возвращает список входящих либо исходящих личных сообщений текущего пользователя.
        /// </summary>
        /// <param name="isGetOutMessage">если этот параметр равен 1, сервер вернет исходящие сообщения. </param>
        /// <param name="offset">смещение, необходимое для выборки определенного подмножества сообщений </param>
        /// <param name="count">количество сообщений, которое необходимо получить.</param>
        /// <param name="timeOffset">максимальное время, прошедшее с момента отправки сообщения до текущего момента в секундах. 0, если Вы хотите получить сообщения любой давности. </param>
        /// <param name="filters">фильтр возвращаемых сообщений: 8 — важные сообщения.</param>
        /// <param name="previewLength">количество символов, по которому нужно обрезать сообщение. Укажите 0, если Вы не хотите обрезать сообщение. (по умолчанию сообщения не обрезаются). Обратите внимание что сообщения обрезаются по словам. </param>
        /// <param name="lastMessageId">идентификатор сообщения, полученного перед тем, которое нужно вернуть последним (при условии, что после него было получено не более count сообщений, иначе необходимо использовать с параметром offset).</param>
        public MessageResponse Get(int isGetOutMessage = 0, int offset = 0, int count = 20, int timeOffset = 0, int filters = 0,
            int previewLength = 0, int lastMessageId = 0)
        {
            var response = RequestHelper.GetMethod<MessageResponse>("messages.get",
                string.Format(
                    "out={0}&offset={1}&count={2}&time_offset={3}&filters={4}&preview_length={5}&last_message_id={6}",
                    isGetOutMessage, offset, count, timeOffset, filters, previewLength, lastMessageId), Token);

            return response;
        }

        /// <summary>
        /// Возвращает список диалогов текущего пользователя.
        /// </summary>
        /// <param name="offset">смещение, необходимое для выборки определенного подмножества сообщений. </param>
        /// <param name="count">количество диалогов, которое необходимо получить. </param>
        /// <param name="previewLength">оличество символов, по которому нужно обрезать сообщение. Укажите 0, если Вы не хотите обрезать сообщение. (по умолчанию сообщения не обрезаются). </param>
        /// <param name="unread">Значение 1 означает, что нужно вернуть только диалоги в которых есть непрочитанные входящие сообщения. По умолчанию 0. </param>
        /// <returns></returns>
        public DialogResponse GetDialogs(uint offset=0, uint count=20, int previewLength=0, int unread = 0)
        {
            var response = RequestHelper.GetMethod<DialogResponse>("messages.getDialogs",
                string.Format(
                    "offset={0}&count={1}&preview_length={2}&unread={3}", offset, count, previewLength, unread
                    ), Token);

            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="domain"></param>
        /// <param name="message"></param>
        /// <param name="attachment"></param>
        /// <returns></returns>
        public int Send(int userId, string domain = "", string message = "", string attachment = "")
        {
            var response = RequestHelper.GetMethod<MessageSendResponse>("messages.send",
                string.Format(
                    "user_id={0}&domain={1}&message={2}&attachment={3}", userId, domain, message, attachment), Token);

            if (response == null)
                return -1;

            return int.Parse(response.Value);
        }

        #region LongPoll Server

        /// <summary>
        /// Возвращает данные, необходимые для подключения к Long Poll серверу.
        /// Long Poll подключение позволит Вам моментально узнавать о приходе новых сообщений и других событий.
        /// </summary>
        /// <param name="usessl">1 — использовать SSL. </param>
        /// <param name="needpts">1 — возвращать поле pts, необходимое для работы метода messages.getLongPollHistory</param>
        public MessagePollServerResponse GetLongPollServer(int usessl = 0, int needpts = 0)
        {
            var response = RequestHelper.GetMethod<MessagePollServerResponse>("messages.getLongPollServer",
                string.Format("use_ssl={0}&need_pts={1}", usessl, needpts), Token);

            return response;
        }

        #endregion
    }
}
