﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hqub.Mellowave.Vkontakte.API.Factories.Wrappers
{
    public class StatusWrapper : BaseWrapper
    {
        public StatusWrapper(string token) : base(token)
        {
        }

        public void Set(string message, string audio)
        {
            var response = RequestHelper.GetMethod("status.set", string.Format("text={0}&audio={1}", message, audio),
                                                   Token);

            System.Diagnostics.Debug.WriteLine(response);
        }
    }
}
