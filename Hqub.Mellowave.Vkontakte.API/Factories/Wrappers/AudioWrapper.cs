﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hqub.Mellowave.Vkontakte.API.Model.Audio;

namespace Hqub.Mellowave.Vkontakte.API.Factories.Wrappers
{
    public class AudioWrapper : BaseWrapper
    {
        public AudioWrapper(string token) : base(token)
        {
        }

        /// <summary>
        /// Метод audio.search
        /// </summary>
        /// <param name="query"></param>
        /// <param name="count"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public AudioResponse Search(string query, int count = 50, int offset = 0)
        {
            var response = RequestHelper.GetMethod<AudioResponse>("audio.search",
                                                                                    string.Format("q={0}&count={1}&offset={2}", query, count, offset), Token);

            return response;
        }

        /// <summary>
        /// Метод audio.get
        /// </summary>
        /// <param name="ownerId"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public AudioResponse Get(string ownerId, int offset = 0, int count = 100)
        {
            var response = RequestHelper.GetMethod<AudioResponse>("audio.get",
                string.Format("owner_id={0}&offset={1}&count={2}", ownerId, offset, count), Token);

            return response;
        }


        /// <summary>
        /// Получаем кол-во треков пользователя
        /// </summary>
        /// <param name="ownerId">id-пользователя</param>
        /// <returns></returns>
        public int GetUserLibraryTrackCount(string ownerId)
        {
            var response = Get(ownerId, 0, 1);

            return response.Count;
        }

        /// <summary>
        /// Метод audio.getById
        /// </summary>
        /// <param name="trackId"></param>
        /// <returns></returns>
        public Audio GetById(string trackId)
        {
            var response = RequestHelper.GetMethod<AudioResponse>("audio.getById", string.Format("audios={0}", trackId),
                Token);

            return response != null && response.Tracks.Count > 0 ? response.Tracks[0] : null;
        }

        public List<Audio> SearchMany(IList<string> tracks)
        {
            const string searchTemplate = "var {argument}=API.audio.search({\"q\":\"{audio}\",\"count\":1});";
            const string returnTemplate = "return {items:[response]};";

            var code = new StringBuilder();

            var countI = 0;
            var arguments = new string[tracks.Count];
            foreach (var track in tracks)
            {
                var argumentName = string.Format("a{0}", countI);
                var template = searchTemplate.Replace("{audio}", track).Replace("{argument}", argumentName);

                arguments[countI] = argumentName;
                code.AppendLine(template);

                ++countI;
            }

            code.AppendLine();
            code.AppendLine(returnTemplate.Replace("[response]",
                string.Format("{0}", string.Join(" %2B ", arguments.Select(a => string.Format("{0}.items", a))))));

            var response = RequestHelper.GetMethod<AudioManyResponse>("execute.xml",
                string.Format("code={0}", code), Token);

            return response.Items;
        }
    }
}
