﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hqub.Mellowave.Vkontakte.API.Factories.Wrappers;

namespace Hqub.Mellowave.Vkontakte.API.Factories
{
    public enum ProductFactoryTypes
    {
        Audio,
        Message,
        Wall,
        Profile,
        Status
    }

    public class ApiFactory
    {
        private static readonly object Lock = new object();
        private static ApiFactory _instance;
        
        private Dictionary<ProductFactoryTypes, BaseWrapper> _products;

        private ApiFactory(string token)
        {
            _products = new Dictionary<ProductFactoryTypes, BaseWrapper>
            {
                {ProductFactoryTypes.Audio, new AudioWrapper(token)},
                {ProductFactoryTypes.Message, new MessageWrapper(token)},
                {ProductFactoryTypes.Profile, new ProfileWrapper(token)},
                {ProductFactoryTypes.Status, new StatusWrapper(token)},
                {ProductFactoryTypes.Wall, new WallWrapper(token)}
            };
        }

        #region Methods

        public BaseWrapper Get(ProductFactoryTypes productType)
        {
            return _products[productType];
        }

        public T Get<T>(ProductFactoryTypes productType) where T : BaseWrapper
        {
            return (T) Get(productType);
        }

        public AudioWrapper GetAudioProduct()
        {
            return Get<AudioWrapper>(ProductFactoryTypes.Audio);
        }

        public ProfileWrapper GetProfileProduct()
        {
            return Get<ProfileWrapper>(ProductFactoryTypes.Profile);
        }

        public StatusWrapper GetStatusProduct()
        {
            return Get<StatusWrapper>(ProductFactoryTypes.Status);
        }

        public MessageWrapper GetMessageProduct()
        {
            return Get<MessageWrapper>(ProductFactoryTypes.Message);
        }

        public WallWrapper GetWallProduct()
        {
            return Get<WallWrapper>(ProductFactoryTypes.Wall);
        }

        #endregion

        /// <summary>
        /// Instance singleton with double checking. Safe-threading.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static ApiFactory Instance(string token = "")
        {
            if (_instance != null) return _instance;

            lock (Lock)
            {
                return _instance ?? (_instance = _instance ?? new ApiFactory(token));
            }
        }
    }
}
