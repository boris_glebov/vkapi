﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Hqub.Mellowave.Vkontakte.API.Model.Message;
using Newtonsoft.Json;

namespace Hqub.Mellowave.Vkontakte.API
{
    public static class RequestHelper
    {
        public static T Get<T>(string url)
        {
            string message = string.Empty;
            try
            {
                var request = System.Net.WebRequest.Create(url);

                var response = request.GetResponse();

                using (var stream = response.GetResponseStream())
                {
                    if (stream == null)
                        throw new NullReferenceException(Localization.Messages.StreamIsEmpty);


                    var xml = new XmlSerializer(typeof (T));
                    message = ToXml(stream);
                    System.Diagnostics.Debug.WriteLine(message);

                    return (T) xml.Deserialize(ToStream(message));
                }
            }
            catch (Exception ex)
            {
                //TODO: Обработать сообщения <error>
                PrintError("Unknow result. " + ex.Message);

                return default(T);
            }
        }

        private static void PrintError(string message)
        {
            System.Diagnostics.Debug.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
        }

        private static string ToXml(Stream stream)
        {
            StreamReader reader = new StreamReader(stream);
            string text = reader.ReadToEnd();

            return text;
        }

        private static MemoryStream ToStream(string xml)
        {
            var memory = new MemoryStream(Encoding.UTF8.GetBytes(xml));
            return memory;
        }

        private static string ToText<T>(T obj)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, obj);
                return textWriter.ToString();
            }
        }

        public static string Get(string url)
        {
            try
            {
                var request = System.Net.WebRequest.Create(url);

                var response = request.GetResponse();

                using (var stream = response.GetResponseStream())
                {
                    if (stream == null)
                        throw new NullReferenceException(Localization.Messages.StreamIsEmpty);

                    using (var streamReader = new StreamReader(stream))
                    {
                        return streamReader.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Unknow result. " + ex.Message);
            }
        }

        public static T GetMethod<T>(string methodName, string methodParametrs, string accessToken)
        {
            return Get<T>(string.Format(Properties.Settings.Default.UrlMethod, methodName, methodParametrs, accessToken));
        }

        public static string GetMethod(string methodName, string methodParametrs, string accessToken)
        {
            return Get(string.Format(Properties.Settings.Default.UrlMethod, methodName, methodParametrs, accessToken));
        }
    }
}
