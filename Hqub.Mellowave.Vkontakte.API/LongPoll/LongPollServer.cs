﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Mellowave.Vkontakte.API.Factories;
using Newtonsoft.Json;

namespace Hqub.Mellowave.Vkontakte.API.LongPoll
{
    public class LongPollServer
    {
        //http://{$server}?act=a_check&key={$key}&ts={$ts}&wait=25&mode=2 
        private readonly string ServerName = "http://{0}?act=a_check&key={1}&ts={2}&wait=25&mode=2 ";

        public enum ResponseCode
        {
            MessageWasRemove = 0,   //  удаление сообщения с указанным local_id
            FlagsChanged = 1,       //  замена флагов
            FlagsSet = 2,           //  установка флагов сообщения (FLAGS|=$mask)
            FlagsReset = 3,         //  сброс флагов сообщения (FLAGS&=~$mask)
            MessageWasReceived = 4,     //  добавление нового сообщения
            MessagesWasReaded = 6,    //  прочтение всех входящих сообщений с $peer_id вплоть до $local_id включительно
            MessagesOutWasReaded = 7,   // прочтение всех исходящих сообщений с $peer_id вплоть до $local_id 
        }


        public LongPollServer(string server, string key, string ts, ApiFactory api, bool isAutoReconnection = true)
        {
            Server = server;
            Key = key;
            Ts = ts;
            IsAutoReconnection = isAutoReconnection;
        }

        public static LongPollServer Connect(ApiFactory api, bool isAutoReconnection = true)
        {
            var factory = api.GetMessageProduct();

            var longPollConnectionInfo = factory.GetLongPollServer();

            var longPollServer = Connect(longPollConnectionInfo.Server, longPollConnectionInfo.Key,
                longPollConnectionInfo.Ts, api);

#if DEBUG
            longPollServer.ReceiveData += data => System.Diagnostics.Debug.WriteLine(data);
#endif

            return longPollServer;
        }

        public static LongPollServer Connect(string server, string key, string ts, ApiFactory api, bool isAutoReconnection = true)
        {
            var longPollServer = new LongPollServer(server, key, ts, api, isAutoReconnection);
            longPollServer.Open();

            return longPollServer;
        }

        #region Public Methods

        private Task<string> OpenAsync()
        {
            var task = new Task<string>(() =>
            {
                var url = string.Format(ServerName, Server, Key, Ts);

                return RequestHelper.Get(url);
            });

            task.Start();

            return task;
        }

        public async void Open()
        {
            var response = await OpenAsync();

            Parse(response);

            Reconnect();
        }

        public void Reconnect()
        {
            Open();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Десериализует и разбирает пришедший ответ
        /// </summary>
        /// <param name="data"></param>
        private void Parse(string data)
        {
            dynamic response = JsonConvert.DeserializeObject(data);

            if (response.failed == 2)
            {
                GetConnectionClosedMessage();
                return;
            }

            // Обновляем значение ts:
            Ts = response.ts;
            InvokeReceiveData(data);

            // Продолжаем разбирать ответ:
            var updates = response.updates;
            if(updates == null || updates.Count == 0)
                return;

            foreach (var update in updates)
            {
                var code = (int) update[0];
                switch ((ResponseCode)code)
                {
                    case ResponseCode.MessageWasReceived:
                        ParseMessageReceved(update);
                        break;
                }

            }
        }

        private void ParseMessageReceved(dynamic update)
        {
            var messageId = (int)update[1];
//            var flags = (int) update[2];
            var fromId = (int) update[3];
            var timestamp = FromTimeStamp((long) update[4]);
            var subject = (string) update[5];
            var text = (string) update[6];

            InvokeReceiveMessage(messageId, fromId, timestamp, subject, text);
        }

        private DateTime FromTimeStamp(long timestamp)
        {
            return (new DateTime(1970, 1, 1, 0, 0, 0, 0)).AddSeconds(timestamp).ToLocalTime();
        }

        private void GetConnectionClosedMessage()
        {
            
        }

        #endregion

        #region Properties

        public bool IsAutoReconnection { get; set; }
        public string Server { get; set; }
        public string Ts { get; set; }
        public string Key { get; set; }
        public ApiFactory Api { get; set; }

        #endregion

        #region Events

        //
        // Событие возникает, когда сервер получает какие либо данные
        public delegate void ReceiveDataHandler(string data);
        public event ReceiveDataHandler ReceiveData;
        private void InvokeReceiveData(string data)
        {
            if (ReceiveData != null)
                ReceiveData(data);
        }

        //
        // Событие возникает, при истечении срока жизни ключа. Требуется переподключение
        public delegate void ConnectionClosedHander();
        public event ConnectionClosedHander ConnectionClosed;
        private void InvokeConnectionClosed()
        {
            if (ConnectionClosed != null)
                ConnectionClosed();
        }

        //
        // Событие возникает при получении нового сообщения
        public delegate void ReceiveMessageHander(
            int messageId, int fromId, DateTime timestamp, string subject, string text);
        public event ReceiveMessageHander ReceiveMessage;
        private void InvokeReceiveMessage(int messageId, int fromId, DateTime timestamp, string subject, string text)
        {
            if (ReceiveMessage != null)
                ReceiveMessage(messageId, fromId, timestamp, subject, text);
        }




        #endregion
    }
}
