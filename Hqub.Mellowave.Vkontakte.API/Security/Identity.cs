﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Hqub.Mellowave.Vkontakte.API.Exceptions;

namespace Hqub.Mellowave.Vkontakte.API.Security
{
    internal class Identity
    {
        private Identity(Model.AuthEntity user)
        {
            User = user;
        }

        private static Identity _identity;

        public static Identity Instance
        {
            get
            {
                if (_identity != null)
                    return _identity;

                _identity = _identity ??
                            new Identity(Auth(Properties.Settings.Default.ClientId,
                                              Properties.Settings.Default.ClientSecret));

                return _identity;
            }
        }

        private static Model.AuthEntity Auth(string clientId, string clientSecurity)
        {
            var url = string.Format("{0}?client_id={1}&client_secret={2}&grant_type=client_credentials",
                                    Properties.Settings.Default.UrlAuth, clientId, clientSecurity);

            var result = RequestHelper.Get<Model.AuthEntity>(url);

            if (result == null)
                throw new AuthFaultException(Localization.Messages.AuthFault);

            return result;
        }

        #region Properties

        public Model.AuthEntity User { get; set; }

        public string CalcSig(string parametrs)
        {
            return Md5Helper.GetMd5Hash(MD5.Create(),
                                                 string.Format("{0}{1}",
                                                               parametrs,
                                                               Properties.Settings.Default.ClientSecret));
        }

        #endregion
    }
}
