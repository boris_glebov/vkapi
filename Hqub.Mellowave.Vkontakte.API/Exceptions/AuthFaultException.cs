﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Mellowave.Vkontakte.API.Exceptions
{
    public class AuthFaultException : Exception
    {
        public AuthFaultException(string message)
            : base(message)
        {

        }
    }
}
