﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Vkontakte.API.Test
{
    [TestClass]
    public class AudoApiTest
    {
        private static string _token =
          "94c302c57ab9b5e759f606188deabbee4a2ead6a4b599e8ebd4b52b7398db224ff9f51d7d2e16d3b94b3d";

        [TestMethod]
        public void TestSearchMany()
        {
            var audio = new Hqub.Mellowave.Vkontakte.API.Factories.Wrappers.AudioWrapper(_token);
            var tracks = audio.SearchMany(new List<string>
            {
                "Король и Шут - Кукла Колдуна",
                "Ozzy Osbourne",
                "The Scorpions"
            });

            Assert.AreEqual(tracks.Count, 3);
        }
    }
}
